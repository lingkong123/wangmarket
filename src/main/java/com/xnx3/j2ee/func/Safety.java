package com.xnx3.j2ee.func;

import com.xnx3.j2ee.util.SafetyUtil;

/**
 * 安全方面操作
 * @author 管雷鸣
 * @deprecated 已废弃，请使用 {@link SafetyUtil}
 */
public class Safety extends SafetyUtil{
	
}
