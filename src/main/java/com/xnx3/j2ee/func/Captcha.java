package com.xnx3.j2ee.func;

import com.xnx3.media.CaptchaUtil;

/**
 * 验证码
 * @author 管雷鸣
 * @deprecated 已废弃，请使用 {@link CaptchaUtil}
 */
public class Captcha extends com.xnx3.j2ee.util.CaptchaUtil{

}
